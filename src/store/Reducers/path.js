const initialState = {
  filename: '',
  name: ''
};

const Path = (state = initialState, action) => {
  const { payload } = action;
  switch (action.type) {
    case 'ADD_FILENAME':
      return {
        ...state,
        filename: payload.pathName,
        name: payload.name
      };
    default:
      return state;
  }
};

export default Path;
