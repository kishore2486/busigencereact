import React from 'react';
import { Container, Row, Col } from 'reactstrap';
import { DragDropContext, Droppable } from 'react-beautiful-dnd';
import Format from './components/Format';
import Source from './components/Source';
import Visualizer from './components/Visualizer';
import styles from './App.module.css';

class App extends React.Component {
  state = {
    csv: [{ id: 11, content: 'File' }, { id: 12, content: 'Transform' }, { id: 13, content: 'CreateTable' }],
    components: [{ id: 1, comp: <Format /> }, { id: 2, comp: <Source /> }, { id: 3, comp: <Visualizer /> }]
  };

  onDragEnd = result => {
    const { destination, source, draggableId } = result;
    const { csv } = this.state;
    console.log(destination);

    if (!destination) {
      return;
    }

    if (destination.droppableId === source.droppableId && destination.index === source.index) {
      // eslint-disable-next-line no-useless-return
      return;
    }

    if (draggableId === 1 && destination.droppableId === 3 && destination.index === 0) {
      const newCsv = [...csv];
      newCsv[0].content = 'File Added';
      this.setState({
        csv: newCsv
      });
    }

    if (draggableId === 11 && destination.droppableId === 3 && destination.index === 1) {
      const newCsv = [...csv];
      if (newCsv[0].content === 'File Added') {
        newCsv[1].content = 'Transformed';
      }
      this.setState({
        csv: newCsv
      });
    }

    if (draggableId === 12 && destination.droppableId === 3 && destination.index === 2) {
      const newCsv = [...csv];
      if (newCsv[1].content === 'Transformed') {
        newCsv[2].content = 'Table Created';
      }
      this.setState({
        csv: newCsv
      });
    }
  };

  render() {
    const { csv, components } = this.state;
    return (
      <React.Fragment>
        <DragDropContext onDragEnd={this.onDragEnd}>
          <Container className={`${styles.heading} shadow-sm p-3 mb-5 bg-white rounded`}>
            <Row>
              <Col>
                <h2>Data Analyzer</h2>
              </Col>
            </Row>
          </Container>
          <Container>
            <Row>
              {components.map(ele => (
                <Droppable droppableId={ele.id} key={ele.id}>
                  {provided => (
                    <div ref={provided.innerRef} {...provided.droppableProps}>
                      {ele.id === 3 ? <Visualizer csv={csv} /> : ele.comp}
                      {provided.placeholder}
                    </div>
                  )}
                </Droppable>
              ))}
            </Row>
          </Container>
        </DragDropContext>
      </React.Fragment>
    );
  }
}

export default App;


/*  Developed by 
  Rama Kishore
  */