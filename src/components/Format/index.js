/* eslint-disable jsx-a11y/no-static-element-interactions */
/* eslint-disable jsx-a11y/click-events-have-key-events */
/* eslint-disable jsx-a11y/label-has-associated-control */
/* eslint-disable jsx-a11y/label-has-for */

import React from 'react';
import {
  Collapse, CardBody, Card, Button, Alert
} from 'reactstrap';
import { connect } from 'react-redux';
import axios from 'axios';
import PropTypes from 'prop-types';
import Wrapper from '../Wrapper';

class Format extends React.Component {
  state = {
    collapse: null,
    path: { value: '', name: ''},
    file: null
  };

  toggle1 = () => {
    this.setState(state => ({ collapse: state.collapse ? null : 1 }));
  };

  toggle2 = () => {
    this.setState(state => ({ collapse: state.collapse ? null : 2 }));
  };

  fileHandler = e => {
    this.setState({
      path: { value: e.target.value, name: e.target.files[0].name },
      file: e.target.files[0]
    });
  };

  handleSubmit = () => {
    const data = new FormData();
    const { file, path } = this.state;
    const { fileNameHandler } = this.props;

    data.append('filecsv', file);
    axios.post('http://localhost:2019/public/uploadcsv', data, {}).then(res => {
      if (res.data.msg === 'uploaded!') {
        fileNameHandler(path.value, path.name);
      }
    });
  };

  render() {
    const { collapse, path } = this.state;
    const { filename } = this.props;
    return (
      <Wrapper title="Select Format">
        <div className="form-check">
          <input className="form-check-input" type="checkbox" value="" id="defaultCheck1" onClick={this.toggle1} />
          <label htmlFor="defaultCheck1" className="form-check-label">
            Mysql
          </label>
        </div>
        <Collapse isOpen={collapse === 1}>
          <Card>
            <CardBody>
              Anim pariatur cliche reprehenderit, enim eiusmod high life accusamus terry richardson ad squid. Nihil anim
              keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident.
            </CardBody>
          </Card>
        </Collapse>
        <div className="form-check">
          <input className="form-check-input" type="checkbox" value="" id="defaultCheck2" onClick={this.toggle2} />
          <label htmlFor="defaultCheck2" className="form-check-label">
            Csv
          </label>
        </div>
        <Collapse isOpen={collapse === 2}>
          <Card>
            <CardBody>
              <div className="custom-file">
                <input
                  type="file"
                  className="custom-file-input"
                  name="filecsv"
                  onChange={this.fileHandler}
                  id="customFile"
                />
                <label className="custom-file-label" htmlFor="customFile">
                  {path.value.length > 0 ? path.value : 'choose file'}
                </label>
              </div>
              <Button color="primary" onClick={this.handleSubmit}>
                Submit
              </Button>
              {filename.length > 0 ? <Alert color="success">File sent to csv source</Alert> : null}
            </CardBody>
          </Card>
        </Collapse>
      </Wrapper>
    );
  }
}

const mapStateToProps = ({ filename }) => ({
  filename
});

const mapDispatchToProps = dispatch => ({
  fileNameHandler: (path, name) => dispatch({ type: 'ADD_FILENAME', payload: { pathName: path, name: name }})
});

Format.propTypes = {
  filename: PropTypes.string.isRequired,
  fileNameHandler: PropTypes.func.isRequired
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Format);
