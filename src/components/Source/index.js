import React from 'react';
import { connect } from 'react-redux';
import {
  TabContent, TabPane, Nav, NavItem, NavLink, Row, Col, Breadcrumb, BreadcrumbItem
} from 'reactstrap';
import { Draggable } from 'react-beautiful-dnd';
import classnames from 'classnames';
import PropTypes from 'prop-types';
import Wrapper from '../Wrapper';

const mapStateToProps = ({ filename }) => ({
  filename
});

class Source extends React.Component {
  constructor(props) {
    super(props);

    this.toggle = this.toggle.bind(this);
    this.state = {
      activeTab: '1'
    };
  }

  toggle(tab) {
    const { activeTab } = this.state;
    if (activeTab !== tab) {
      this.setState({
        activeTab: tab
      });
    }
  }

  render() {
    const { filename } = this.props;
    const { activeTab } = this.state;
    return (
      <Wrapper title="Select Source">
        <Nav tabs>
          <NavItem>
            <NavLink
              className={classnames({ active: activeTab === '1' })}
              onClick={() => {
                this.toggle('1');
              }}
            >
              Mysql
            </NavLink>
          </NavItem>
          <NavItem>
            <NavLink
              className={classnames({ active: activeTab === '2' })}
              onClick={() => {
                this.toggle('2');
              }}
            >
              Csv
            </NavLink>
          </NavItem>
        </Nav>
        <TabContent activeTab={activeTab}>
          <TabPane tabId="1">
            <Row>
              <Col sm="12">
                <h4>Tab 1 Contents</h4>
              </Col>
            </Row>
          </TabPane>
          <TabPane tabId="2">
            <Row>
              <Col sm="12">
                <Draggable draggableId={1} index={1}>
                  {provided => (
                    <div {...provided.draggableProps} {...provided.dragHandleProps} ref={provided.innerRef}>
                      {filename.length > 0 ? (
                        <Breadcrumb>
                          <BreadcrumbItem active>{filename}</BreadcrumbItem>
                        </Breadcrumb>
                      ) : null}
                    </div>
                  )}
                </Draggable>
              </Col>
            </Row>
          </TabPane>
        </TabContent>
      </Wrapper>
    );
  }
}

Source.propTypes = {
  filename: PropTypes.string.isRequired
};

export default connect(mapStateToProps)(Source);
