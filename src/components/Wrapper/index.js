import React from 'react';
import { Col } from 'reactstrap';
import PropTypes from 'prop-types';
import styles from './wrapper.module.css';

const Wrapper = ({ title, children }) => (
  <Col className={styles.wrap}>
    <h3 className={`${styles.title} shadow-sm p-3 mb-5 bg-white rounded`}>{title}</h3>
    {children}
  </Col>
);

Wrapper.propTypes = {
  title: PropTypes.string.isRequired,
  children: PropTypes.node.isRequired
};

export default Wrapper;
