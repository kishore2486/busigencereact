import React from 'react';
import { Draggable } from 'react-beautiful-dnd';
import { connect } from 'react-redux';
import axios from 'axios';
import PropTypes from 'prop-types';
import Wrapper from '../Wrapper';
import styles from './visualizer.module.css';

const mapStateToProps = ({ filename, name }) => ({
  filename,
  name
});

class Visualizer extends React.Component {

  componentWillReceiveProps() {
     const { csv, name } =  this.props;
     if(csv[2].content === 'Table Created') {
      axios.post('http://localhost:2019/public/createTable', { "path": name }, {}).then(res => {
        console.log(res);
      }).catch(err => {
        console.log(err);
      });
     }
  }

  render() {
    const { csv, filename } = this.props;
    return (
      <Wrapper title="Visualizer">
        {filename.length > 0 ? (
          <React.Fragment>
            {csv.map((ele, index) => (
              <Draggable draggableId={ele.id} key={ele.id} index={index}>
                {provided => (
                  <div
                    className={styles.container}
                    ref={provided.innerRef}
                    {...provided.draggableProps}
                    {...provided.dragHandleProps}
                  >
                    <div className={styles.dropBoxes}>
                      <h5>{ele.content}</h5>
                    </div>
                  </div>
                )}
              </Draggable>
            ))}
            <button type="button" className={`btn btn-primary ${styles.btnManu}`}>
              Run The mapping
            </button>
          </React.Fragment>
        ) : null}
      </Wrapper>
    );
  }
}

Visualizer.defaultProps = {
  csv: [{ id: 11, content: 'File' }, { id: 12, content: 'Transform' }, { id: 13, content: 'Create Table' }]
};

Visualizer.propTypes = {
  csv: PropTypes.array,
  filename: PropTypes.string.isRequired,
  name: PropTypes.string.isRequired
};

export default connect(mapStateToProps)(Visualizer);
